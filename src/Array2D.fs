// Array2D
// A partial portable reimplementation of Microsoft.FSharp.Collections.Array2D with a bit of extra stuff.
// This exists because Fable does not provide its own.
// Internally it is based on arrays of arrays.
// Contains length1, length2, get, set, copy, create, mapi and map.
// Also contains rows, an extra one.

module Array2D

type Array2D<'a> = Array2D of 'a[][]

let private unwrap (Array2D(arr)) = arr

let inline length1 (Array2D(arr)) = Array.length arr
let length2 (Array2D(arr)) =
    if Array.length arr > 0 then
        Array.length arr.[0]
    else
        0

let get (Array2D(arr)) x y = arr.[x].[y]
let set (Array2D(arr)) x y v = arr.[x].[y] <- v

let inline copy (Array2D(arr)) = Array.map Array.copy arr |> Array2D

let create w h v = Array.create w (Array.create h v) |> Array2D

let mapi f =
    unwrap
    >> Array.mapi (fun x col -> Array.mapi (fun y v -> f x y v) col)
    >> Array2D

let map f = unwrap >> Array.map (Array.map(f)) >> Array2D

let rows = unwrap