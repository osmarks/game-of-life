// Contains functions for loading and rendering patterns.
module IO

open Sim

// Contains ASCII-art IO.
module AsciiArt =
    let cellToChar = function
        | On -> 'X'
        | Off -> 'O'

    let charToCell = function
        | 'X' -> On
        | 'O' -> Off
        | c -> failwithf "%A is not a valid cell character" c

    let showBoard b =
        Array2D.map cellToChar b
        |> Array2D.rows
        |> Array.map (fun cs -> System.String(cs))
        |> String.concat "\n"

    let readBoard (str:string) =
        str.Split '\n'
        |> Array.filter (fun str -> str.Length > 0)
        |> Array.map (fun s -> s.ToCharArray())
        |> Array2D.Array2D
        |> Array2D.map charToCell