// Contains a collection of interesting patterns
module Patterns

open Sim
open IO.AsciiArt

// The glider: a simple construction which moves diagonally forever.
let glider = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOXXXOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOXOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOXOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

// Lightweight spaceship: moves horizontally
let spaceship = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOXXXXOOOOOOOOOOOOOOOOOO
OOOOOOOOOOXOOOXOOOOOOOOOOOOOOOOO
OOOOOOOOOOXOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOXOOXOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

// A collection of static objects
let stillLifes = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOXOOXOOOO
OOXOOOOXXOOOOOOXOOOOOOOOXXXXOOOO
OXOXOOOXXOOOOOXOXOOOOOOOOOOOOOOO
OOXOOOOOOOOOOOXOXOOOOOOOXXXXOOOO
OOOOOOOOOOOOOOOXOOOOOOOOXOOXOOOO
OOOOOOOXXOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOXOXOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOXXOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOXOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOXOXOOOOOOOOOOOOOO
OOXXOOOOOOOOOOXOOXOOOOOOOOOOOOOO
OOXOXOOOOOOOOOOXXOOOOOOOOOOOOOOO
OOOXOOOOOOOOOOOOOOOOOOOOOOOXXOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOXOOXOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOXXOXO
OOOOOOOOXXOOOOOOOOOOOOOOOOOOOOXO
OOOOOOOXOOXOOOOOOOOOOOOOOOOOOOXX
OOOOOOOXOOXOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOXXOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOXXOXXOOOOOOOOOOOOXXOOOOOOOOOOO
OOXOOOXOOOOOOOOOOOOXOXOOOOOOOOOO
OOOXXXOOOOOOOOOOOOOOOXOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOXXOOOOOOOOO
OOOXXXOOOOOOOOOOOOOOOOOOOOOOOOOO
OOXOOOXOOOOOOOOOOOOOOOOOOOOOOOOO
OOXXOXXOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

let oscillators1 = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOXOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOXOXOOOOOOOOOOOOOOOOOOOOOOOOO
OOOXOOOXOOOOOOOOOOOOOOOOOOOOOOOO
OOOOXOOOXOOOOOOOOOOOXOXOOXOXOOOO
OOOOOXOOOXOOOOOOXXOXOOXOOXOOXOXX
OOOOOOXOOOXOOOOOOOOOXOXOOXOXOOOO
OOOOOOOXOXOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOXOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOXXOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOXOOXOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOXOXOOOOOOO
OOOOOOOOOOOOOOOOOOOOXXOXOOOOOOOO
OOOOOOOOOOOOOOOOOOOOXXXOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOXXOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOXOOXOOOOOOOOOOOOOOOOOOOOOOOOOO
OOXOOXOOOOOOOOOOOOOOOOOOOOOOOOOO
XXOXXOXXOOOOOOXXOOOOXXOOOOOOOOOO
OOXOOXOOOOOOOOXOXOOXOXOOOOOOOOOO
OOXOOXOOOOOOOOOOXOOXOOOOOOXXXOOO
XXOXXOXXOOOOOOXOXOOXOXOOOOOOOOOO
OOXOOXOOOOOOOOXXOOOOXXOOOOOOOOOO
OOXOOXOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

let copperhead = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOXXOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOXXXXOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOXXXXXXOOOOOOOOOOOOOO
OOOOOOOOOOOOOXXXXOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOXXOOXXOOOOOOOOOOOOOO
OOOOOOOOOOXXOXOOXOXXOOOOOOOOOOOO
OOOOOOOOOOOOOXOOXOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOXXOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOXXOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

let trafficLight = readBoard """
OOOOO
OOOOO
OXXXO
OOOOO
OOOOO
"""

let oscillators2 = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOXXXOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOXXXOOOXXXOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOXOOOOXXXOOOXXXOOOOXOOOOOO
OOOOOOOXOOOOOOOOOOOOOOOOOXOOOOOO
OOOOOOOXOOOOXXXOOOXXXOOOOXOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOOOOXOOOOXOXOOOOXOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOXXXOOOXXXOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOXXXOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

// Thanks to Naveen Naru
let golly = readBoard """
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOXXOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOXXXXOOOOOOOOOOOOOOO
OOOOOOOOOOOOXOXXOXXOXOOOOOOOOOOO
OOOOOOOOOOOOOOOOOXXOOXOOOOOOOOOO
OOOOOOOOOOOOXXOOXXOOXXXOOOOOOOOO
OOOOOOOOOOOOXXXXOXOOXXXOOOOOOOOO
OOOOOOOOOOOXOOXOOOXOOXOOOOOOOOOO
OOOOOOOOOOXXXOOXOXXXXOOOOOOOOOOO
OOOOOOOOOOXXXOOXXOOXXOOOOOOOOOOO
OOOOOOOOOOOXOOXXOOOOOOOOOOOOOOOO
OOOOOOOOOOOOXOXXOXXOXOOOOOOOOOOO
OOOOOOOOOOOOOOOOXXXXOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOXXOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
"""

let patterns: Board[] = [|
    glider
    spaceship
    stillLifes
    oscillators1
    copperhead
    trafficLight
    golly
|]

let pickRandom (arr: 'a[]) =
    let rnd = System.Random()
    arr.[rnd.Next(Array.length arr)]

let randomPattern () = pickRandom patterns