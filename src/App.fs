module GameOfLife

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import.Browser

open Fable.Helpers.React.Props
open Fable.Helpers.React

open Elmish
open Elmish.React

open Sim

type Message = TogglePause
               | ToggleCell of Position 
               | Step 
               | Clear 
               | Tick 
               | AsciiIOChange of string
               | Export
               | Import
               | PickRandom

type Model = 
    {
    board : Board
    generations : int
    paused : bool
    asciiIOContents : string
    }

let mapBoard f m = { m with board = f m.board }

let mapPaused f m = { m with paused = f m.paused }

let mapGenerations f m =
    { m with generations = f m.generations }

let startingBoard = Array2D.create 32 32 Off
let init() = {board = startingBoard; generations = 0; paused = true; asciiIOContents = ""}, Cmd.none

let update msg model =
    match msg with
    | ToggleCell pos -> mapBoard (invertCellAt pos) model, Cmd.none
    | Step -> mapBoard gameOfLife model |> mapGenerations (fun i -> i + 1), Cmd.none
    | TogglePause -> mapPaused not model, Cmd.none
    | Clear -> 
        {model with board = startingBoard}
        |> mapGenerations (fun _ -> 0)
        , Cmd.none
    | Tick ->
        if not model.paused then
            model, Cmd.ofMsg Step
        else
            model, Cmd.none
    | AsciiIOChange newValue ->
        {model with asciiIOContents = newValue}, Cmd.none
    | Export ->
        {model with asciiIOContents = IO.AsciiArt.showBoard model.board}, Cmd.none
    | Import ->
        {model with board = IO.AsciiArt.readBoard model.asciiIOContents}, Cmd.none
    | PickRandom ->
        {model with board = Patterns.randomPattern()}, Cmd.none

let cellToTd dispatch x y = function
    | On -> td [ClassName "cell on"; OnClick (fun e -> dispatch <| ToggleCell (x, y))] []
    | Off -> td [ClassName "cell off"; OnClick(fun e -> dispatch <| ToggleCell (x, y))] []

let viewGameTable dispatch board =
    let tableCells = Array2D.mapi (cellToTd dispatch) board
    let rows = Array2D.rows tableCells
               |> Array.map (fun row -> tr [] (List.ofArray row))
               |> List.ofArray

    table [CellSpacing (U2.Case1 0.); ClassName "grid"] [tbody [] rows]

let controlButton onclick name =
    button [ClassName "control"; OnClick onclick] [str name]

let view model dispatch =
    let pauseOrPlay =
        match model.paused with
        | true -> "Play"
        | false -> "Pause"

    div [ClassName "life-container"] [
        div [ClassName "buttons"] [
            controlButton (fun e -> dispatch Step) "Step"
            controlButton (fun e -> dispatch TogglePause) pauseOrPlay
            controlButton (fun e -> dispatch Clear) "Clear"
            controlButton (fun e -> dispatch Import) "Import"
            controlButton (fun e -> dispatch Export) "Export"
            controlButton (fun e -> dispatch PickRandom) "Random"
        ]

        div [ClassName "generations"] [sprintf "Generations: %d" model.generations |> str]

        viewGameTable dispatch model.board

        textarea [
            Id "ascii-io"
            ClassName "ascii-io"
            OnChange (fun e -> !!e.target?value |> AsciiIOChange |> dispatch)
            Value (U2.Case1 model.asciiIOContents)
            Rows 40.
            OnClick (fun e -> e.target?select() |> ignore)
        ] []
    ]

let tick initial =
    let sub dispatch =
        window.setInterval((fun _ -> dispatch Tick), 20) |> ignore

    Cmd.ofSub sub

Program.mkProgram init update view
|> Program.withSubscription tick
|> Program.withReact "app"
|> Program.run