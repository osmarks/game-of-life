// Contains types and functions related to the Game of Life
// (John Conway's, not the board game.)
module Sim

type Cell = Off | On
type Position = int * int
type Board = Array2D.Array2D<Cell>

let inline getCell b (x, y) = Array2D.get b x y
let inline setCell b (x, y) v =
    let copy = Array2D.copy b
    Array2D.set copy x y v
    copy

let inline addPos (x1, y1) (x2, y2) =
    (x1 + x2, y1 + y2)

let invertCell = function
    | On -> Off
    | Off -> On

let invertCellAt p board =
    getCell board p
    |> invertCell
    |> setCell board p

let relativeNeighbourPositions = [
    (1, 0)
    (1, 1)
    (0, 1)
    (-1, 0)
    (-1, -1)
    (0, -1)
    (1, -1)
    (-1, 1)
    ]

let rec overflow min max = function
    | n when n >= min && n < max -> n
    | n when n >= max -> n % max
    | n when n < min -> overflow min max (n + max)
    | _ -> failwith "Should not occur"

let wraparound w h (x, y) = (overflow 0 w x, overflow 0 h y)

let neighbours b pos =
    let w = Array2D.length1 b
    let h = Array2D.length2 b
    
    // Get the positions of all the neighbours of the cell
    let absoluteNeighbourPositions = List.map (addPos pos >> wraparound w h) relativeNeighbourPositions
    // Now read all the neighbours from the grid
    
    List.map (getCell b) absoluteNeighbourPositions

let numLive = List.filter (fun c -> c = On) >> List.length

let applyRule (rule: 'a * 'a list -> 'b) board : Array2D.Array2D<'b> = Array2D.mapi (fun x y c -> c, neighbours board (x, y)) board |> Array2D.map rule

let gameOfLifeRule (center, neighbours) =
    let liveNeighbours = numLive neighbours

    match center with
    | On when liveNeighbours = 2 || liveNeighbours = 3 -> On
    | Off when liveNeighbours = 3 -> On
    | _ -> Off

let gameOfLife = applyRule gameOfLifeRule