# Game of Life
A simple and seemingly rather inefficient implementation of John Conway's Game of Life.
It uses [Fable](https://fable.io) and Elmish to run in any reasonably modern browser.

## Running
~~~~ bash
cd src
dotnet fable npm-start # or yarn-start
# The project is now being served on port 8080
~~~~

## Requirements

* [dotnet SDK](https://www.microsoft.com/net/download/core) 1.0.4 or higher
* [node.js](https://nodejs.org) 4.8.2 or higher
* A JS package manager: [yarn](https://yarnpkg.com) or [npm](http://npmjs.com/)